package com.maf.anotherdpexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnotherdpexampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnotherdpexampleApplication.class, args);
	}

}
